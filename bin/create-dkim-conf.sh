#!/bin/bash

set -e

source .env

VOLUME_PATH=./volume
OPENDKIM_PATH=$VOLUME_PATH/opendkim

mkdir -p $OPENDKIM_PATH

# configure OpenDKIM
cat <<EOT > $OPENDKIM_PATH/opendkim.conf
UserID              root
Socket              inet:8891
PidFile             /var/run/opendkim/opendkim.pid
Mode                sv
Syslog              yes
SyslogSuccess       yes
LogWhy              yes
Umask               002
Canonicalization    relaxed/relaxed
MinimumKeyBits      1024
RequireSafeKeys     yes
SendReports         yes
ReportAddress       postmaster@postfix
KeyTable            refile:/etc/opendkim/KeyTable
SigningTable        refile:/etc/opendkim/SigningTable
ExternalIgnoreList  refile:/etc/opendkim/TrustedHosts
InternalHosts       refile:/etc/opendkim/TrustedHosts
EOT

# register to the signing table
echo "*@$SUBDOMAIN.$DOMAINNAME $DOMAINNAME" \
  > $OPENDKIM_PATH/SigningTable

# register to the key table
echo "$DOMAINNAME $DOMAINNAME:$SUBDOMAIN:/etc/opendkim/keys/$SUBDOMAIN.private" \
  > $OPENDKIM_PATH/KeyTable

# add trusted host
cat <<EOT > $VOLUME_PATH/TrustedHosts
127.0.0.1
::1
$SUBDOMAIN.$DOMAINNAME
EOT
