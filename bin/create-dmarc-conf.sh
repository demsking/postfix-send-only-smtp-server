#!/bin/bash

set -e

source .env

VOLUME_PATH=./volume
OPENDMARC_PATH=$VOLUME_PATH/opendmarc

mkdir -p $OPENDMARC_PATH

# configure OpenDMARC
cat <<EOT > $OPENDMARC_PATH/opendmarc.conf
UserID                root
AuthservID            $SUBDOMAIN.$DOMAINNAME
AutoRestart           true
FailureReportsSentBy  postmaster@postfix
FailureReportsBcc     postmaster@postfix
FailureReports        false
HistoryFile           /etc/opendmarc/logs/history.log
IgnoreHosts           /etc/opendmarc/ignore.hosts
PidFile               /var/run/opendmarc.pid
RejectFailures        true
Socket                inet:8893
Syslog                true
TrustedAuthservIDs    $SUBDOMAIN.$DOMAINNAME
UMask                 0002
EOT
