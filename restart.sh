#!/bin/sh

set -e

echo "Restarting containers..."
docker-compose down
docker-compose up -d

echo "Configuring postfix container"
docker exec -it postfix /tmp/init.sh

docker-compose logs -f
